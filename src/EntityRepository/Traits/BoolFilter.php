<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\TableBundle\EntityRepository\Traits;

use Doctrine\ORM\QueryBuilder;
use LS\TableBundle\Model\FilterInterface;

trait BoolFilter
{
    /**
     * @param QueryBuilder $qb
     * @param string $queryStr
     * @param FilterInterface $filter
     * @param string $field
     */
    public function applyBoolFilter(QueryBuilder $qb, $queryStr, FilterInterface $filter, $field)
    {
        $method = 'get' . ucfirst($field);

        if (!is_string($filter->$method())) {
            $qb
                ->andWhere($queryStr . ' = :' . $field)
                ->setParameter($field, $filter->$method());
        }
    }
}
