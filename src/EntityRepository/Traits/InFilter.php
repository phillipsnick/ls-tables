<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\TableBundle\EntityRepository\Traits;

use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use LS\TableBundle\Model\FilterInterface;

trait InFilter
{
    /**
     * @param QueryBuilder $qb
     * @param string $queryStr
     * @param FilterInterface $filter
     * @param string $field
     */
    public function applyInFilter(QueryBuilder $qb, $queryStr, FilterInterface $filter, $field)
    {
        $method = 'get' . ucfirst($field);

        if (!empty($filter->$method())) {
            $expr = new Expr();

            $qb
                ->andWhere($expr->in($queryStr, ':' . $field))
                ->setParameter($field, $filter->$method());
        }
    }
}
