<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\TableBundle\EntityRepository\Traits;

use Doctrine\ORM\QueryBuilder;
use LS\TableBundle\Model\FilterInterface;

trait LikeFilter
{
    /**
     * @param QueryBuilder $qb
     * @param string $queryStr
     * @param FilterInterface $filter
     * @param string $field
     */
    public function applyLikeFilter(QueryBuilder $qb, $queryStr, FilterInterface $filter, $field)
    {
        $method = 'get' . ucfirst($field);

        if (!empty($filter->$method())) {
            $qb
                ->andWhere($queryStr . ' LIKE :' . $field)
                ->setParameter($field, '%' . $filter->$method() . '%');
        }
    }
}
