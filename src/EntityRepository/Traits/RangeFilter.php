<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\TableBundle\EntityRepository\Traits;

use Doctrine\ORM\QueryBuilder;
use LS\TableBundle\Model\FilterInterface;

trait RangeFilter
{
    /**
     * @param QueryBuilder $qb
     * @param string $queryStr
     * @param FilterInterface $filter
     * @param string $field
     */
    public function applyRangeFilter(QueryBuilder $qb, $queryStr, FilterInterface $filter, $field)
    {
        $fromMethod = 'get' . ucfirst($field) . 'From';
        $toMethod = 'get' . ucfirst($field) . 'To';

        if (!empty($filter->$fromMethod())) {
            $qb
                ->andWhere($queryStr . ' > :' . $field . 'From')
                ->setParameter($field . 'From', $filter->$fromMethod());
        }

        if (!empty($filter->$toMethod())) {
            $qb
                ->andWhere($queryStr . ' < :' . $field . 'To')
                ->setParameter($field . 'To', $filter->$toMethod());
        }
    }
}
