<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\TableBundle\Exception;

class OrderByMissingQueryException extends \Exception
{
}
