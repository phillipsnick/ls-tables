<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\TableBundle\Exception;

class OrderByOptionNotExistException extends \Exception
{
}
