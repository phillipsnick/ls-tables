<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\TableBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LSTableBundle extends Bundle
{
}
