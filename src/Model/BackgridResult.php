<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\TableBundle\Model;

use JMS\Serializer\Annotation as Serializer;

class BackgridResult implements ResultInterface
{
    /**
     * @var array
     *
     * @Serializer\Groups({"filter_result"})
     */
    protected $results;

    /**
     * @var int
     *
     * @Serializer\Groups({"filter_result"})
     */
    protected $total;

    /**
     * {@inheritdoc}
     */
    public function setResults(array $results)
    {
        $this->results = $results;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * {@inheritdoc}
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        return $this->total;
    }
}
