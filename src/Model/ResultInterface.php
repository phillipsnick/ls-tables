<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\TableBundle\Model;

interface ResultInterface
{
    /**
     * @param array $results
     * @return $this
     */
    public function setResults(array $results);

    /**
     * @return array
     */
    public function getResults();

    /**
     * @param int $count
     * @return $this
     */
    public function setTotal($count);

    /**
     * @return int
     */
    public function getTotal();
}
