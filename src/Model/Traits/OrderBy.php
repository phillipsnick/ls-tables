<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\TableBundle\Model\Traits;

use LS\TableBundle\Exception\OrderByDefaultUndefinedException;
use LS\TableBundle\Exception\OrderByOptionNotExistException;
use LS\TableBundle\Exception\OrderByMissingQueryException;
use LS\TableBundle\Exception\OrderByOptionsMissingException;

/**
 * You must set orderByOptions in the construct on the class that uses this trait, example:
 *
 * public function __construct()
 * {
 *     $this->setOrderByOptions([
 *         'storeId' => [
 *             'query' => 's.storeId'
 *         ],
 *         'createdAt' => [
 *              'query' => 's.storeId'
 *              'default' => 'ASC'
 *         ]
 *     ];
 * }
 */
trait OrderBy
{
    /**
     * @var array
     */
    protected $orderByOptions;

    /**
     * @var array
     */
    protected $directionOptions = [
        'asc' => 'ASC',
        'desc' => 'DESC'
    ];

    /**
     * @var string|null
     */
    protected $orderBy;

    /**
     * @var string
     */
    protected $direction = 'asc';

    /**
     * @var string
     */
    protected $query;

    /**
     * @param array $orderByOptions
     * @return $this
     */
    public function setOrderByOptions(array $orderByOptions)
    {
        $this->orderByOptions = $orderByOptions;
        return $this;
    }

    /**
     * @return array
     * @throws OrderByOptionsMissingException
     */
    public function getOrderByOptions()
    {
        if (!is_array($this->orderByOptions)) {
            throw new OrderByOptionsMissingException();
        }

        return $this->orderByOptions;
    }

    /**
     * @return array
     */
    public function getOrderByOptionsSelect()
    {
        $options = [];

        foreach ($this->getOrderByOptions() as $key => $value) {
            if (!isset($value['label'])) {
                $label = $key;
            } else {
                $label = $value['label'];
            }

            $options[$label] = $key;

            if (isset($value['default'])) {
                $this->direction = $value['default'];
            }
        }

        return $options;
    }

    /**
     * @param string $orderBy
     * @return $this
     * @throws OrderByOptionNotExistException
     * @throws OrderByMissingQueryException
     */
    public function setOrderBy($orderBy)
    {
        if (!array_key_exists($orderBy, $this->orderByOptions)) {
            throw new OrderByOptionNotExistException($orderBy);
        }

        if (!array_key_exists('query', $this->orderByOptions[$orderBy])) {
            throw new OrderByMissingQueryException($orderBy);
        }

        $this->orderBy = $orderBy;
        $this->query = $this->orderByOptions[$orderBy]['query'];

        return $this;
    }

    /**
     * @return bool
     */
    public function hasOrderBy()
    {
        return $this->orderBy !== null;
    }

    /**
     * @return string
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param string $direction
     * @return $this
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
        return $this;
    }

    /**
     * @return string
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @return array
     */
    public function getDirectionOptionsSelect()
    {
        return array_flip($this->directionOptions);
    }

    /**
     * @return string
     */
    public function getOrderByQuery()
    {
        if (is_null($this->orderBy)) {
            return $this->getOrderByOptions()[$this->getOrderByDefault()]['query'];
        }

        return $this->query;
    }

    /**
     * @return string
     * @throws OrderByDefaultUndefinedException
     */
    public function getOrderByDefault()
    {
        foreach ($this->getOrderByOptions() as $key => $value) {
            if (isset($value['default'])) {
                return $key;
            }
        }

        throw new OrderByDefaultUndefinedException();
    }

    /**
     * @return mixed
     * @throws OrderByDefaultUndefinedException
     * @throws OrderByOptionsMissingException
     */
    public function getOrderByDefaultDirection()
    {
        foreach ($this->getOrderByOptions() as $key => $value) {
            if (isset($value['default'])) {
                return $value['default'];
            }
        }

        throw new OrderByDefaultUndefinedException();
    }
}
