<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\TableBundle\Model\Traits;

trait Page
{
    /**
     * @var int
     */
    protected $page = 1;

    /**
     * @var int
     */
    protected $resultsPerPage = 25;

    /**
     * @param int $page
     * @return $this
     */
    public function setPage($page)
    {
        $page = (int) $page;

        if (is_null($page) || $page < 1) {
            $page = 1;
        }

        $this->page = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $perPage
     * @return $this
     */
    public function setResultsPerPage($perPage)
    {
        $perPage = (int) $perPage;

        if (is_null($perPage) || $perPage < 1) {
            $perPage = 25;
        }

        $this->resultsPerPage = $perPage;
        return $this;
    }

    /**
     * @return int
     */
    public function getResultsPerPage()
    {
        return $this->resultsPerPage;
    }

    /**
     * @return int
     */
    public function getFirstResult()
    {
        return $this->resultsPerPage * ($this->page - 1);
    }
}
