<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\TableBundle\Paginator;

use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use LS\TableBundle\Model\BackgridResult;
use LS\TableBundle\Model\ResultInterface;
use LS\TableBundle\Model\FilterInterface;

class Paginator extends DoctrinePaginator
{
    /**
     * @var FilterInterface
     */
    protected $filter;

    /**
     * @var \ArrayIterator
     */
    protected $iterator;

    /**
     * @param FilterInterface $filter
     * @return $this
     */
    public function setFilter(FilterInterface $filter)
    {
        $this->filter = $filter;
        return $this;
    }

    /**
     * @return FilterInterface
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        if (is_null($this->iterator)) {
            $this->iterator = parent::getIterator();
        }

        return $this->iterator;
    }

    /**
     * @param ResultInterface|null $result
     * @return ResultInterface
     */
    public function getResult(ResultInterface $result = null)
    {
        if (is_null($result)) {
            $result = new BackgridResult();
        }

        $result
            ->setResults($this->getIterator()->getArrayCopy())
            ->setTotal($this->count());

        return $result;
    }
}
