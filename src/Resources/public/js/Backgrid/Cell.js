(function () {
    /**
     * Use HTML instead of Text
     */
    Backgrid.SimpleCell = Backgrid.Cell.extend({
        render: function () {
            this.$el
                .empty()
                .html(this.formatter.fromRaw(this.model.get(this.column.get("name")), this.model));
            this.delegateEvents();
            return this;
        }
    });
})();
