(function () {
    /**
     * Base filter which is generic to input/select
     */
    Backgrid.Extension.Filter = Backbone.View.extend({
        events: {
            change: 'inputChange',
            keyup: 'inputChange'
        },

        initialize: function (options) {
            _.bindAll(this, 'render', 'inputChange');

            this.column = options.column;
            this.collection = options.collection;
            this.attributes = _.extend({}, this.attributes, options.baseAttributes, this.overrideAttributes);

            var value = this.collection.filter.get(this.attributes.name);
            if (typeof value !== 'undefined') {
                this.value = value;
            }

            this.$el.attr(this.attributes);
        },

        inputChange: function (event) {

            var elm = $(event.currentTarget);
            this.collection.filter.set(elm.attr('name'), elm.val());
        },

        render: function() {
            this.$el
                .empty()
                .attr('value', this.value);

            return this;
        }
    });

    /**
     * Text filter with Metronic classes
     */
    Backgrid.Extension.TextFilter = Backgrid.Extension.Filter.extend({
        tagName: 'input',
        className: 'form-filter input-sm form-control',
    });

    /**
     * Number filter
     */
    Backgrid.Extension.NumberFilter = Backgrid.Extension.TextFilter.extend({
        attributes: function () {
            return {
                type: 'number'
            }
        }
    });

    /**
     * Select filter with Metronic classes, note you must extend this and define some options
     */
        // TODO: this should support values from the filter
    Backgrid.Extension.SelectFilter = Backgrid.Extension.Filter.extend({
        tagName: 'select',
        className: 'form-filter input-sm form-control',
        placeholder: null,
        options: {},

        isSelected: function (val) {
            var selected = false;

            _.each(this.collection.filter.get(this.attributes.name), function(item) {
                if (item == val) {
                    selected = true;
                }
            });

            return selected;
        },

        render: function () {
            var self = this;

            this.$el.empty();

            if (this.placeholder) {
                this.renderItem(this.placeholder, '', true);
            }

            if (this.options instanceof Backbone.Collection) {
                this.options.each(function(model) {
                    self.renderItem(model.getLabel(), model.get(model.idAttribute), false, model);
                });
            } else {
                _.each(this.options, this.renderItem, this);
            }

            this.delegateEvents();
            return this;
        },

        renderItem: function (label, id, disabled) {
            this.$el.append($("<option/>", {
                value: id,
                text: label,
                selected: this.isSelected(id),
            }));
        }
    });

    /**
     * AJAX typeahead filter
     */
    Backgrid.Extension.TypeaheadFilter = Backgrid.Extension.Filter.extend({
        // select2 must be wrapped in a div or it won't display
        tagName: 'div',
        delay: 250,
        allowClear: true,
        placeholder: 'All',

        // TODO: support multiple?
        inputChange: function (event) {
            var elm = $(event.currentTarget);
            this.collection.filter.set(elm.attr('name'), elm.find('option').attr('value'));
        },

        render: function () {
            var self = this;

            this.$el
                .empty()
                .append($('<select\>', {
                    name: this.attributes.name,
                    width: this.column.attributes.attributes.width
                }));

            this.$('select').select2({
                allowClear: this.allowClear,
                placeholder: this.placeholder,
                ajax: {
                    url: this.url,
                    delay: this.delay,
                    data: function (params) {
                        return {
                            query: params.term,
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: self.processResults(data.results),
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    }
                },
                minimumInputLength: 1
            });

            this.delegateEvents();
            return this;
        }
    }),

    /**
     * Apply a from and to date picker
     */
    Backgrid.Extension.DateRangeFilter = Backgrid.Extension.Filter.extend({
        template: _.template(
            '<div class="input-group date date-picker<% if (typeof(className) !== "undefined") { %> <%= className %><% } %>" data-date-format="dd/mm/yyyy">' +
            '   <input type="text" class="form-control form-filter input-sm" name="<%= name %>" placeholder="<%= placeholder %>">' +
            '   <span class="input-group-btn"><button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button></span>' +
            '</div>'
        ),

        events: {
            'change input' : 'inputChange'
        },

        inputChange: function (event) {
            var elm = $(event.currentTarget);
            this.collection.filter.set(elm.attr('name'), elm.val());

            if (elm.parent().hasClass('from')) {
                this.$el.find('.to').datepicker('setStartDate', elm.val());
            }

            if (elm.parent().hasClass('to')) {
                this.$el.find('.from').datepicker('setEndDate', elm.val());
            }
        },

        // TODO: this should support values from the filter
        render: function() {
            this.$el
                .empty()
                .append(this.template({
                    className: 'margin-bottom-5 from',
                    name: this.attributes.name + 'From',
                    placeholder: 'From'
                }))
                .append(this.template({
                    className: 'to',
                    name: this.attributes.name + 'To',
                    placeholder: 'To'
                }));

            this.$el.find('.date-picker').datepicker({
                orientation: 'bottom',
                autoclose: true
            });

            return this;
        }
    });

    /**
     * Setup the header row cell and the filter if any has been defined
     */
    Backgrid.Extension.HeaderFilterCell = Backbone.View.extend({
        tagName: 'td',

        initialize: function (options) {
            this.column = options.column;
            this.collection = options.collection;
            this.filter = this.column.get('filter');

            if (_.isUndefined(this.filter)) {
                return;
            }

            this.filter = Backgrid.resolveNameToClass(this.filter, 'Filter');
            this.filter = new this.filter({
                column: this.column,
                collection: this.collection,
                baseAttributes: {
                    name: this.column.get('name')
                }
            });
        },

        render: function () {
            this.$el.empty();

            if (this.filter) {
                this.$el.append(this.filter.render().el);
            }

            return this;
        }
    });

    /**
     *
     */
    Backgrid.Extension.FilterHeaderRow = Backgrid.Row.extend({
        className: 'filter',
        requiredOptions: ['columns', 'collection'],

        makeCell: function (column, options) {
            var filterCell = column.get('filterCell') || options.filterCell || Backgrid.Extension.HeaderFilterCell;

            return new filterCell({
                column: column,
                collection: this.collection,
                attributes: column.attributes.attributes // bit of a hack to allow column widths to be set
            });
        }
    });

    /**
     * This gives us the ability to override the original header and inject our filter row
     */
    Backgrid.Extension.FilterHeader = Backgrid.Header.extend({
        initialize: function (options) {
            Backgrid.Header.prototype.initialize.apply(this, arguments);

            this.rowFilter = new Backgrid.Extension.FilterHeaderRow({
                columns: this.columns,
                collection: this.collection
            });
        },

        render: function () {
            // hack to add the Metronic heading class
            this.row.render().$el.addClass('heading');

            this.$el
                .append(this.row.$el)
                .append(this.rowFilter.render().$el);

            this.delegateEvents();
            return this;
        }
    });
})();
