(function () {
    Backgrid.Extension.FilteredTable = Backbone.View.extend({
        className: 'backgrid-container',

        initialize: function(options) {
            // parse the columns and apply the defaults used throughout LS projects.
            options.columns = this.parseColumns(options.columns);

            this.grid = new Backgrid.Extension.FilteredGrid(options);
            this.footer = new Backgrid.Extension.Footer({
                collection: options.collection
            });
        },

        parseColumns: function(columns) {
            //TODO: this should probably support instance of columns
            _.each(columns, function (column, key) {
                if (typeof column['cell'] === 'undefined') {
                    columns[key]['cell'] = 'string';
                }

                if (typeof column['editable'] === 'undefined') {
                    columns[key]['editable'] = false;
                }

                if (typeof column['sortable'] === 'undefined') {
                    columns[key]['sortable'] = false;
                }

                if (typeof column['label'] === 'undefined') {
                    columns[key]['label'] = column['name'].charAt(0).toUpperCase() + column['name'].slice(1);
                }

                if (typeof column['template'] !== 'undefined') {
                    var template = column['template'];
                    if (typeof template === 'string') {
                        template = _.template($(template).html());
                    }

                    columns[key]['cell'] = Backgrid.Cell.extend({
                        render: function () {
                            this.$el.empty().append(template(this.model.toJSON())); //TODO: should it pass the whole model too?
                            return this;
                        }
                    });
                }
            });

            return columns;
        },

        render: function () {
            this.$el
                .empty()
                .append(this.grid.render().el)
                .append(this.footer.render().el);

            return this
        }
    });

    Backgrid.Extension.FilteredGrid = Backgrid.Grid.extend({
        className: 'table table-striped table-hover',
        emptyText: 'No results match your filters.',
        sortType: 'toggle',
        header: Backgrid.Extension.FilterHeader,

        initialize: function (options) {
            Backgrid.Grid.prototype.initialize.apply(this, arguments);

            this.debounce = _.debounce(function () {
                Metronic.startPageLoading();
            }, 200);

            this.listenTo(options.collection.filter, 'change', function () {
                this.displayLoading();

                if (!_.isUndefined(options.history) && false !== options.history) {
                    this.updateHistory(options.history);
                }
            }.bind(this));
            this.listenTo(options.collection, 'request', this.displayLoading);
            this.listenTo(options.collection, 'sync', this.hideLoading);
            this.listenTo(options.collection, 'error', this.hideLoading);
        },

        displayLoading: function () {
            this.debounce();
        },

        hideLoading: function () {
            this.debounce.clear();
            Metronic.stopPageLoading();
        },

        updateHistory: function (url) {
            Backbone.history.navigate(url + '?' + $.param(this.collection.filter.toJSON()));
        }
    });
})();
