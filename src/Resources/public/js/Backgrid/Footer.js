(function () {
    /**
     * Common footer summary
     */
    Backgrid.Extension.FooterSummary = Backbone.View.extend({
        className: 'col-md-6',
        options: {
            template: _.template('<p style="padding-top: 15px;">Showing <%= showing %> of <%= total %></p>')
        },

        initialize: function () {
            this.listenTo(this.collection, 'add', this.render);
            this.listenTo(this.collection, 'remove', this.render);
            this.listenTo(this.collection, 'reset', this.render);
            this.listenTo(this.collection, 'backgrid:sorted', this.render);
        },

        render: function () {
            var total = this.collection.state.totalRecords;

            this.$el.empty();

            if (total > 0) {
                this.$el.append(this.options.template({
                    showing: this.collection.length,
                    total: total
                }));
            }

            return this;
        }
    });

    /**
     * Row containing summary and pagination
     */
    Backgrid.Extension.Footer = Backbone.View.extend({
        className: 'row',

        initialize: function (options) {
            this.footerSummary = new Backgrid.Extension.FooterSummary({
                collection: options.collection
            });

            this.paginator = new Backgrid.Extension.PaginatorV2({
                collection: options.collection
            });
        },

        render: function () {
            this.$el
                .empty()
                .append(this.footerSummary.render().$el)
                .append(this.paginator.render().$el);

            return this;
        }
    })
})();
