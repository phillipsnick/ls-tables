(function () {
    /**
     * Override the paginator to support placing it inside a col-md-6 container
     */
    Backgrid.Extension.PaginatorV2 = Backgrid.Extension.Paginator.extend({
        className: 'col-md-6',
        ulClass: 'pagination pagination-sm pull-right',

        /**
         * Add the ability to specify a ul class and by default set to the Metronic one.
         */
        render: function () {
            this.$el.empty();

            if (this.handles) {
                for (var i = 0, l = this.handles.length; i < l; i++) {
                    this.handles[i].remove();
                }
            }

            var handles = this.handles = this.makeHandles();
            var ul = document.createElement("ul");
            var i;
            ul.className = this.ulClass;

            for (i = 0; i < handles.length; i++) {
                ul.appendChild(handles[i].render().el);

            }

            this.el.appendChild(ul);

            return this;
        }
    });
})();
