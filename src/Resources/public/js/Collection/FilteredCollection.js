(function () {
    'use strict';

    /**
     * Extend this class and define:
     *  * filterModel - Backbone.Model instance for the tables filter
     *  * model - Backbone.Model instance for each of the results
     *  * url - String to API endpoint
     */
    Backbone.FilteredCollection = Backbone.PageableCollection.extend({
        /**
         * Defaults to be overridden
         */
        filterModel: null,

        /**
         * Default settings
         */
        type: 'POST',
        state: {},
        queryParams: {
            pageSize: 'resultsPerPage',
            sortKey: 'orderBy',
            order: 'direction',
            totalPages: null,
            totalRecords: null
        },

        initialize: function (models, options) {
            var self = this;

            this.options = options || {};

            if (typeof this.options.filter === 'undefined') {
                this.filter = new this.filterModel;
            } else {

                this.filter = this.options.filter;
            }

            this.queryParams = _.extend(this.queryParams, this.filter.toJSON());

            this.listenTo(this.filter, 'change', _.debounce(function (filter) {
                self.state.currentPage = 1;
                self.queryParams = _.extend(self.queryParams, filter.toJSON());
                self.fetch();
            }, 300));
        },

        /**
         * Intercept the default fetch to always use reset:true
         *
         * @param options
         */
        fetch: function (options) {
            options = _.extend({}, options, {reset: true});

            Backbone.PageableCollection.prototype.fetch.apply(this, options);
        },

        parseState: function (res, queryParams, state, options) {
            return {
                totalRecords: res.total
            };
        },

        parseRecords: function (res, options) {
            return res.results;
        }
    });
})();
